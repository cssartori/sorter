# **Sorter** #

This program implements several sorting methods, allowing the user to compare two different algorithms through three criteria: execution time, number of swaps and number of comparisons. The methods available are:

1. Bubble Sort;
2. Insertion Sort;
3. Selection Sort;
4. Quick Sort;
5. Heap Sort;
6. Merge Sort;
7. Counting Sort;

After the calculations are done, a graphic is presented to better compare the results. One can also save the detailed results to a text file.

## **Implementation** ##

Implemented in C++11, using Qt Creator to generate the GUI. The graphic plotting tool used is the QCustomPlot ([link](http://www.qcustomplot.com/)). It can be compiled both in Windows and Linux using Qt (> 4.0) tools.

## **About** ##

Done for the INF01124 - Data Search and Classification discipline, 2013, at UFRGS.